stackname = CoinApi

all: build package deploy

build:
	aws s3 mb s3://chrisaddy-coinapi --region us-east-1
	sam build

package:
	sam package \
		--template-file template.yaml \
		--output-template output.yaml \
		--s3-bucket chrisaddy-coinapi \
		--region us-east-1 \
		--debug

deploy:
	sam deploy \
		--template-file output.yaml \
		--stack-name $(stackname) \
		--capabilities CAPABILITY_IAM \
		--region us-east-1

teardown:
	aws cloudformation delete-stack --stack-name $(stackname)
