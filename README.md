# Coin Scraper


## Batch Script

The batch script to gather the last year's data and store into a DynamoDB table can be run in the gather_data.ipynb jupyter notebook. I chose this format since I can easily mix code with plain text. I find it easier to be able to mix notes into the code to describe my thinking at each stage of the process.


## The microservice

This microservice can be invoked at `https://be038ly1h2.execute-api.us-east-1.amazonaws.com/Prod/` with a POST request of the form:

```json
{
  "date": "2019-10-22"
}
```

This will update the database with the pricing information from the Coin API for Bitcoin, Etherium, Ripple, and Dogecoin for that date.

