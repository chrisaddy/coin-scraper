import json
import requests


coin_api_key = os.environ['COINAPIKEY']


def send_response(code, message):
    return {
        'statusCode': code,
        'body': json.dumps(message)
    }


def create_url(coin, date):
    return f'https://rest.coinapi.io/v1/ohlcv/{coin}/USD/history?period_id=1DAY&time_start={date}&time_end={date}'


def coin_history(coin, date):
    btc = requests.get(
        url = create_url(coin, date),
        headers={
            'X-CoinAPI-Key': coin_api_key,
            'content-type': 'application/json', # required according to documentation
        }
    )
    return json.loads(r.text)


def lambda_handler(event, context):
    date = json.loads(event['body'])['date']
    btc = coin_history('BTC', date)

    return send_response(200, btc)
